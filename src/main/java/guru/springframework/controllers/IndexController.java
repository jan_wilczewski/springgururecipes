package guru.springframework.controllers;

import guru.springframework.domain.Category;
import guru.springframework.domain.UnitOfMeasure;
import guru.springframework.repositories.CategoryRepository;
import guru.springframework.repositories.UnitOfMeasureRepository;
import guru.springframework.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

/**
 * Created by jt on 6/1/17.
 */
@Controller
public class IndexController {

    @Autowired
    private RecipeService recipeService;

    @RequestMapping({"", "/", "/index"})
    public String getIndexPage(Model model) {

        model.addAttribute("recipes", recipeService.getRecipes());

        return "index";
    }




    //    @Autowired
//    private CategoryRepository categoryRepository;
//    @Autowired
//    private UnitOfMeasureRepository unitOfMeasureRepository;


//    @RequestMapping({"", "/", "/index"})
//    public String getIndexPage(){
//        Optional<Category> categoryOptional = categoryRepository.findByDescription("Italian");
//        Optional<UnitOfMeasure> unitOfMeasureOptional = unitOfMeasureRepository.findByDescription("Teaspoon");
//        System.out.println("Cat id is: " + categoryOptional.get().getId());
//        System.out.println("UOM id is: " + unitOfMeasureOptional.get().getId());
//        return "index";
//    }
}
